# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

FILESEXTRAPATHS:prepend := "${THISDIR}/ot-br-posix:"

SRCREV = "ec5f57bf7c05eb81fb104b38cb82995dc48465fd"

SRC_URI:append = " \
                  file://60-otbr-ip-forward.conf \
                  file://otbr-configuration \
                  file://otbr-configuration.service \
                  file://91-otbr-rcp.rules \
                  file://otbr-agent.default \
                 "

do_install:append() {
    install -d ${D}${sysconfdir}/sysctl.d/
    install -m 0644 ${WORKDIR}/60-otbr-ip-forward.conf ${D}${sysconfdir}/sysctl.d/
    install -D ${WORKDIR}/otbr-configuration ${D}${sbindir}/
    install -D -m 0644 ${WORKDIR}/otbr-configuration.service ${D}${systemd_unitdir}/system/
    install -m 0755 ${WORKDIR}/build/tools/pskc ${D}${sbindir}/
    install -d ${D}${sysconfdir}/udev/rules.d/
    install -m 0644 ${WORKDIR}/91-otbr-rcp.rules ${D}${sysconfdir}/udev/rules.d/
    install -d ${D}${sysconfdir}/default/
    install -m 0644 ${WORKDIR}/otbr-agent.default ${D}${sysconfdir}/default/otbr-agent
}

SYSTEMD_SERVICE:${PN} += " otbr-configuration.service"
